.PHONY: quick
.PHONY: all
.PHONY: clean

quick: main.tex
	xelatex $^

all: main.tex
	xelatex main.tex
	bibtex main
	xelatex main.tex
	xelatex main.tex

clean:
	rm -f *.aux *.bbl *.blg *.log *.out *.synctex.gz *.nav *.snm *.toc *.vrb
